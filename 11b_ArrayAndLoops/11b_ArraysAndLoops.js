// Q1
const array = ["hello","world","search","good","world","search","good"];
let searchIndex = -1;
for (let index = 0; index < array.length; index++) {
  const element = array[index];
  if (element === "search") {
    searchIndex = index;
  }
}
console.log(searchIndex);

// Q2
let searchIndex2 = -1;
for (let index = 0; index < array.length; index++) {
  const element = array[index];
  if (element === "search") {
    searchIndex2 = index;
    break;
  }
}
console.log(searchIndex2);

// Q3
function findIndex(array, string) {
  for (let index = 0; index < array.length; index++) {
    const element = array[index];
    if (element === string) {
      return index;
    }
  }
  return -1;
}
console.log(findIndex(array, "bad"));

// Q4
const groceries = ["apple","egg","egg","mango","orange","pineapple","egg","egg","mango","orange"];
function removeEgg(array) {
  const resultArray = [];
  for (let index = 0; index < array.length; index++) {
    const element = array[index];
    if (element !== "egg") {
      resultArray.push(element);
    }
  }
  return resultArray;
}
console.log(removeEgg(groceries));

// Q5
function removeFirstTwoEggs(array) {
  const resultArray = [];
  let removedEggCount = 0;
  for (let index = 0; index < array.length; index++) {
    const element = array[index];
    if (element === "egg" && removedEggCount < 2) {
      removedEggCount++;
    }else{
      resultArray.push(element);
    }
  }
  return resultArray;
}
console.log(removeFirstTwoEggs(groceries));

// Q6
function removeLastTwoEggs(array) {
  const resultArray = [];
  let removedEggCount = 0;
  array = array.reverse();
  for (let index = 0; index < array.length; index++) {
    const element = array[index];
    if (element === "egg" && removedEggCount < 2) {
      removedEggCount++;
    }else{
      resultArray.push(element);
    }
  }
  return resultArray.reverse();
}
console.log(removeLastTwoEggs(groceries));

// Q7
function removeLastTwoEggsWithSlice(array) {
  const resultArray = [];
  let removedEggCount = 0;
  const slicedArray = array.reverse().slice();
  for (let index = 0; index < slicedArray.length; index++) {
    const element = array[index];
    if (element === "egg" && removedEggCount < 2) {
      removedEggCount++;
    }else{
      resultArray.push(element);
    }
  }
  return resultArray.reverse();
}
console.log(removeLastTwoEggsWithSlice(groceries));
console.log(groceries);

// Q8
for (let index = 1; index <= 20; index++) {
  if (index % 3 === 0 && index % 5 === 0) {
    console.log("FizzBuzz");
  }else if (index % 5 === 0) {
    console.log("Buzz");
  }else if (index % 3 === 0) {
    console.log("Fizz");
  }else{
    console.log(index);
  }
}

// Q9
function unique(groceries) {
  const uniques = new Set(groceries);
  return Array.from(uniques);
}
console.log(unique(groceries));

