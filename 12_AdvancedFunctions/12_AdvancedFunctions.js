// Q1
const add = () => {
  console.log(2 + 3);
}
add();
add();

// Q2
const runTwice = (add) => {
  add();
  add();
}
runTwice(add)

// Q3, Q4
function onFinish() {
  setTimeout(() => {
    const btnContainer = document.querySelector('#btn-finish');
    btnContainer.innerHTML = "Finished!";
  }, 3000);
  document.querySelector('#btn-finish').innerHTML = "Loading...";
}

// Q5
var prevId;
function onAddToCart() {
  const para = document.querySelector('#added-text');
  para.innerHTML = "Added!";
  clearTimeout(prevId);
  prevId = setTimeout(() => {
    para.innerHTML = "";
  }, 2000)
}

// Q6
let noOfMsg = 2;
setInterval(() => {
  const title = document.title;
  if (title === 'App' && noOfMsg > 0) {
    title = `(${noOfMsg}) New messages`;
  } else {
    title = "App"
  }
}, 1000);


// Q7
function changeMsg(bool) {
  if (bool) {
    noOfMsg++;
  } else if (noOfMsg > 0) {
    noOfMsg--;
  } 
}

// Q8, Q9
const multiply = (num1, num2) => num1 * num2;
console.log(multiply(3,4));

// Q10
const array = [2,33,34,-21,53,-56,35,-6,2];
let sum = 0;
const countPositive = (array) => {
  array.forEach((element) => {
    if (element > 0) {
      sum++;
    }
  });
  return sum;
}
console.log(countPositive(array));

// Q11
const addNum = (array, num) => {
  return array.map((element) => {
    return element + num;
  })
}
console.log(addNum(array, 2));

// Q12
const foods = ["apple","mango","egg","orange","egg","orange","egg"];
const removeEggs = (foods) => {
  return foods.filter((element) => {
    return element !== "egg";
  })
}
console.log(removeEggs(foods));

// Q13
const removeFirstTwoEggs = (foods) => {
  let count = 0;
  return foods.filter((element) => {
    element === "egg" ? count++ : count; 
    return count < 3 ? element !== "egg" : element;
  })
}
console.log(removeFirstTwoEggs(foods));